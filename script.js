
const root = document.querySelector('#root');
const BASE_URL = 'https://ajax.test-danit.com/api/swapi/films';

class Request {
    get(url) {
        return fetch(url).then((data) => {
            return data.json();
        }).catch((error) => {
            throw error('Fetch request error');
        })
    }
}

class Film {
    getCharacters(characters) {
        const charactersRequests = characters.map((character) => {
            return request.get(character);
        })
        return Promise.allSettled([...charactersRequests]);
    }

    renderFilms(films) {
        const filmsList = document.createElement('ul');

        const items = films.map(({episodeId, name, openingCrawl, characters}) => {

            const listItem = document.createElement('li');

            const filmTitle = document.createElement('p');
            const filmEpisode = document.createElement('p');
            const filmDescription = document.createElement('p');
            const heroes = document.createElement('p');

            filmTitle.textContent = `Фільм: ${name}`;
            filmEpisode.textContent = `Епізод: ${episodeId}`;
            filmDescription.textContent = `Опис: ${openingCrawl}`;
            heroes.textContent = 'Персонажі:';

            listItem.append(filmTitle, filmEpisode, filmDescription, heroes, this.showLoader());

            this.getCharacters(characters).then((data) => {

                listItem.removeChild(listItem.lastChild);

                let charactersList = document.createElement('ul');

                let characterItems = data.map(({value}) => {
                    let listItem = document.createElement('li')
                    listItem.textContent = value.name;

                    return listItem;
                })

                charactersList.append(...characterItems);
                listItem.append(charactersList);
            });

            return listItem;
        })

        filmsList.append(...items);

        return filmsList;
    }

    showLoader() {
        const loader = document.createElement('div');
        loader.classList.add('loader');

        return loader;
    }

}


const request = new Request();
const films = new Film();


request.get(BASE_URL)
    .then((data) => root.append(films.renderFilms(data)))
    .catch((error) => console.log(error));

